# pyura

A field-based experiment was conducted to investigate how the presence of Pyura can modify natural intertidal rocky-shore communities through competitive exclusion (encroachment) of native species.  Experiments focused 1) on the effect of Pyura on the green-lipped mussel (Perna canaliculus), an iconic and key rocky-shore native species within the low intertidal zone; and 2) on the establishment of rocky shore assemblages within patches of differing Pyura densities. Both experiments were conducted at two sites in the Tauroa Peninsula, Northland region in New Zealand.
